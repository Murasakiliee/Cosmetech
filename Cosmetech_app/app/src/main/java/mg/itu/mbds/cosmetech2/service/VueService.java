package mg.itu.mbds.cosmetech2.service;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;

import mg.itu.mbds.cosmetech2.utilitaire.Constante;
import mg.itu.mbds.cosmetech2.vue.Connexion;

/**
 * Created by Murielle on 28/04/2017.
 */
public class VueService {
    public void testConnexion(Activity activity){
        if (!checkConnexion(activity)){
            erreurConnexion(activity);
        }
    }

    public boolean checkConnexion(Activity activity){
        int ID = getIdutilisateur(activity);
        if (ID > 0) return true;
        return false;
    }

    public void erreurConnexion(Activity activity){
        //Demander a l'utilisateur de se connecter
        Intent i = new Intent(activity, Connexion.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(i);
    }

    public void deconnexion (Activity activity){
        SharedPreferences settings = activity.getSharedPreferences(Constante.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("id", 0);
        editor.putInt("idPanier", 0);
        editor.commit();
    }

    public int getIdutilisateur(Activity activity){
        SharedPreferences settings = activity.getSharedPreferences(Constante.PREFS_NAME, 0);
        int ID = settings.getInt("id", 0);
        return ID;
    }

    public int getIdpanier(Activity activity){
        SharedPreferences settings = activity.getSharedPreferences(Constante.PREFS_NAME, 0);
        int ID = settings.getInt("idPanier", 0);
        return ID;
    }

    public void setIdutilisateur (Activity activity, int id){
        SharedPreferences settings = activity.getSharedPreferences(Constante.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("id", id);
        editor.commit();
    }
    public void setIdpanier (Activity activity, int id){
        SharedPreferences settings = activity.getSharedPreferences(Constante.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("idPanier", id);
        editor.commit();
    }

    public boolean checkHavePanier(Activity activity){
        if(getIdpanier(activity) > 0) return true;
        return false;
    }
}
