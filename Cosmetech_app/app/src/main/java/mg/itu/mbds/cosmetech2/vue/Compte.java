package mg.itu.mbds.cosmetech2.vue;

import android.app.Activity;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import mg.itu.mbds.cosmetech2.R;
import mg.itu.mbds.cosmetech2.modele.Utilisateur;
import mg.itu.mbds.cosmetech2.service.UtilisateurService;
import mg.itu.mbds.cosmetech2.service.VueService;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Compte.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Compte} factory method to
 * create an instance of this fragment.
 */
public class Compte extends Fragment {
    // TODO: Rename parameter arguments, choose names that match

    private TextView txtNom;
    private TextView txtPrenom;
    private TextView txtPseudo;
    private TextView txtSolde;
    private VueService serviceG = new VueService();

    private OnFragmentInteractionListener mListener;

    public static Compte newInstance() {
        return new Compte();
    }

    public Compte() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        serviceG.testConnexion((MainActivity) getActivity());


        new UtilisateurService().findUserById(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hello, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(1);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        txtNom = (TextView) this.getActivity().findViewById(R.id.nom);
        txtPrenom = (TextView) this.getActivity().findViewById(R.id.prenom);
        txtPseudo = (TextView) this.getActivity().findViewById(R.id.pseudo);
        txtSolde = (TextView) this.getActivity().findViewById(R.id.solde);
    }

    public void setView(Utilisateur utilisateur){
        txtNom.setText(utilisateur.getNom());
        txtPrenom.setText(utilisateur.getPrenom());
        txtPseudo.setText(utilisateur.getPseudo());
        new UtilisateurService().getSolde(this);
    }

    public void setSolde(double solde){
        txtSolde.setText(new Double(solde).toString());
    }
}
