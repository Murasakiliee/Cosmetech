package mg.itu.mbds.cosmetech2.service;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;

import mg.itu.mbds.cosmetech2.modele.Achat;
import mg.itu.mbds.cosmetech2.utilitaire.ConstanteService;
import mg.itu.mbds.cosmetech2.vue.ListeAchat;

/**
 * Created by Murielle on 29/04/2017.
 */
public class AchatService {
    private VueService serviceG = new VueService();

    public void getAllAchats (final ListeAchat liste){
        RequestQueue rq = Volley.newRequestQueue(liste.getActivity());
        StringRequest postReq = new StringRequest(Request.Method.GET, ConstanteService.allAchats, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    Log.e("TEST VOLLEY", "getAllAchats " + response);
                    ObjectMapper mapper = new ObjectMapper();
                    Achat[] achats = mapper.readValue(response, Achat[].class);
                    liste.setData(achats);
                }
                catch(Exception e){
                    Log.e("TEST VOLLEY", "erreur : "+e.getMessage());
                    //erreur
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //erreur

            }
        })  {
        };
        rq.add(postReq);
    }

    public void enregistrerLivraison (final ListeAchat liste, int idpanier){
        RequestQueue rq = Volley.newRequestQueue(liste.getActivity());
        StringRequest postReq = new StringRequest(Request.Method.GET, ConstanteService.enregistrerLivraison+"?idpanier="+idpanier, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    liste.shuffle();
                }
                catch(Exception e){
                    Log.e("TEST VOLLEY", "erreur : "+e.getMessage());
                    //erreur
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //erreur

            }
        })  {
        };
        rq.add(postReq);
    }
}
