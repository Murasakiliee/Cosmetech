package mg.itu.mbds.cosmetech2.vue.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mg.itu.mbds.cosmetech2.R;
import mg.itu.mbds.cosmetech2.modele.LigneCommande;

/**
 * Created by Murielle on 28/04/2017.
 */
public class ProduitAdapter extends BaseAdapter{
    private ArrayList<LigneCommande> items;
    Context mContext;

    public ProduitAdapter(ArrayList<LigneCommande> data, Context context) {
        this.mContext = context;
        this.items = data;
    }

    @Override
    public int getCount() {
        return items.size(); //returns total of items in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // inflate the layout for each list row
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).
                    inflate(R.layout.template_produit, parent, false);
        }

        // get current item to be displayed
        LigneCommande currentItem = (LigneCommande) getItem(position);

        // get the TextView for item name and item description
        TextView textViewItemName = (TextView)
                convertView.findViewById(R.id.nomProduit);
        TextView textViewItemDescription = (TextView)
                convertView.findViewById(R.id.categorieProduit);
        TextView prixProduit = (TextView)
                convertView.findViewById(R.id.prixProduit);
        TextView quantiteProduit = (TextView)
                convertView.findViewById(R.id.quantiteProduit);

        //sets the text for item name and item description from the current item object
        textViewItemName.setText(currentItem.getNom());
        prixProduit.setText("Prix : "+currentItem.getPrix());
        quantiteProduit.setText("Quantite : "+currentItem.getQuantite());
        double montant = currentItem.getQuantite()*currentItem.getPrix();
        textViewItemDescription.setText("Montant : "+montant);
        // returns the view for the current row
        return convertView;
    }
}
