package mg.itu.mbds.cosmetech2.vue;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.tech.NfcF;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;

import mg.itu.mbds.cosmetech2.R;
import mg.itu.mbds.cosmetech2.service.PanierService;
import mg.itu.mbds.cosmetech2.service.UtilisateurService;
import mg.itu.mbds.cosmetech2.service.VueService;
import mg.itu.mbds.cosmetech2.utilitaire.NfcUtilitaire;

public class MainActivity extends YouTubeBaseActivity
implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private VueService serviceG = new VueService();

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    NfcAdapter nfcAdapter = null;
    PendingIntent pendingIntent = null;
    NfcUtilitaire utilitaireNFC = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        serviceG.testConnexion(this);

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        utilitaireNFC = new NfcUtilitaire();
        if (!nfcAdapter.isEnabled())
        {
            //Demander à l’utilisateur d’activer l’option NFC
            Toast.makeText(this, "Veuillez activer votre NFC", Toast.LENGTH_LONG).show();
        }
        else{
            processNfcIntent(this.getIntent()) ;
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = getHomeFragment(position);
        if (fragment == null){
            deconnexion();
        }
        else{
            fragmentTransaction.replace(R.id.container, fragment).commit();
        }
    }

    private Fragment getHomeFragment(int position) {
        switch (position) {
            case 0:
                Compte homeFragment = Compte.newInstance();
                return homeFragment;
            case 1:
                ListeAchat photosFragment = ListeAchat.newInstance();
                return photosFragment;
            case 2:
                Panier produit = new Panier();
                return produit;
            default :
                return null;
        }
    }

    public void deconnexion(){
        new UtilisateurService().deconnexion(this);
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.menu1);
                break;
            case 2:
                mTitle = getString(R.string.menu2);
                break;
            case 3:
                mTitle = getString(R.string.menu3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        serviceG.testConnexion(this);
        processNfcIntent(intent) ;
    }

    public void processNfcIntent (Intent intent)
    {
        if (intent != null && NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            String[] msgs = utilitaireNFC.getData(intent);
            if (msgs[0].compareTo("Produit") == 0) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Produit produit = Produit.newInstance(new Integer(msgs[1]));
                fragmentTransaction.replace(R.id.container, produit).commit();
            } else {
                //création panier
                PanierService service = new PanierService();
                service.creerPanier(this);
            }
        }
    }

    public void onPause() {
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }

    public void onResume() {
        super.onResume();
        IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            ndef.addDataType("*/*");
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("fail", e);
        }
        IntentFilter[] intentFiltersArray = new IntentFilter[] {ndef, };
        String[][] techListsArray = new String[][] { new String[] { NfcF.class.getName() } };
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFiltersArray,
                techListsArray);
    }
}
