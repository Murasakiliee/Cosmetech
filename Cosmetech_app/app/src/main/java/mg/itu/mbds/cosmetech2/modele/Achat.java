package mg.itu.mbds.cosmetech2.modele;

import java.util.Date;

/**
 * Created by Murielle on 29/04/2017.
 */
public class Achat {
    private int id;
    private Date dateachat;
    private double montant;
    private int statut;

    public Achat(){

    }

    public Achat(Date dateachat, double montant, int id, int statut) {
        this.dateachat = dateachat;
        this.montant = montant;
        this.id = id;
        this.statut = statut;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateachat() {
        return dateachat;
    }

    public void setDateachat(Date dateachat) {
        this.dateachat = dateachat;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }
}
