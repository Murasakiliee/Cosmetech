package mg.itu.mbds.cosmetech2.vue;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import mg.itu.mbds.cosmetech2.R;
import mg.itu.mbds.cosmetech2.modele.Utilisateur;
import mg.itu.mbds.cosmetech2.service.UtilisateurService;
import mg.itu.mbds.cosmetech2.service.VueService;

public class Connexion extends Activity {

    private EditText pseudoEditText;
    private EditText passEditText;
    private VueService serviceG = new VueService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(serviceG.checkConnexion(this)){
            Intent i = new Intent(this, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            this.startActivity(i);
        }
        setContentView(R.layout.activity_connexion);
        pseudoEditText = (EditText) findViewById(R.id.username);
        passEditText = (EditText) findViewById(R.id.password);
    }

    public void checkLogin(View arg0) {

        String pseudo = pseudoEditText.getText().toString();
        String pass = passEditText.getText().toString();

        if(isValid(pseudo, pass))
        {
            // Validation Completed
            Utilisateur utilisateur = new Utilisateur(pseudo, pass);
            UtilisateurService service = new UtilisateurService();
            service.connexion(this, utilisateur);
        }
    }

    private boolean isValid (String pseudo, String pass){
        if (pseudo == null || pseudo.compareTo("")==0) return false;
        if (pass == null || pass.compareTo("")==0) return false;
        return true;
    }

    public void erreurConnexion(){
        Toast.makeText(this, "Desole, Connexion echoue", Toast.LENGTH_LONG).show();
    }
}
