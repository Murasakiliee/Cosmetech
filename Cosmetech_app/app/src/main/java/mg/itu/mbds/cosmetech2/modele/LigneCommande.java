package mg.itu.mbds.cosmetech2.modele;

/**
 * Created by Murielle on 21/04/2017.
 */
public class LigneCommande {
    private int idcommande;
    private int idproduit;
    private String nom;
    private double prix;
    private double quantite;

    public LigneCommande(){

    }
    public LigneCommande(int idcommande, int idproduit, String nom, double prix, double quantite) {
        this.idcommande = idcommande;
        this.idproduit = idproduit;
        this.nom = nom;
        this.prix = prix;
        this.quantite = quantite;
    }

    public int getIdcommande() {
        return idcommande;
    }

    public void setIdcommande(int idcommande) {
        this.idcommande = idcommande;
    }

    public int getIdproduit() {
        return idproduit;
    }

    public void setIdproduit(int idproduit) {
        this.idproduit = idproduit;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public double getQuantite() {
        return quantite;
    }

    public void setQuantite(double quantite) {
        this.quantite = quantite;
    }
}
