package mg.itu.mbds.cosmetech2.service;

import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

import mg.itu.mbds.cosmetech2.modele.Utilisateur;
import mg.itu.mbds.cosmetech2.utilitaire.Constante;
import mg.itu.mbds.cosmetech2.utilitaire.ConstanteService;
import mg.itu.mbds.cosmetech2.vue.Compte;
import mg.itu.mbds.cosmetech2.vue.Connexion;
import mg.itu.mbds.cosmetech2.vue.MainActivity;

/**
 * Created by Murielle on 06/04/2017.
 */
public class UtilisateurService {
    private VueService serviceG = new VueService();
    public void connexion(final Connexion activity, final Utilisateur utilisateur) {

        RequestQueue rq = Volley.newRequestQueue(activity);
        StringRequest postReq = new StringRequest(Request.Method.POST, ConstanteService.connexion, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    ObjectMapper mapper = new ObjectMapper();
                    Utilisateur utilisateur1 = mapper.readValue(response, Utilisateur.class);
                    serviceG.setIdutilisateur(activity, utilisateur1.getId());
                    Intent i = new Intent(activity, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(i);
                }
                catch(Exception e){
                    Log.e("TEST VOLLEY", "erreur : "+e.getMessage());
                    activity.erreurConnexion();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                activity.erreurConnexion();

            }
        })  {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap();
                params.put("login", utilisateur.getPseudo());
                params.put("mdp", utilisateur.getMotDePasse());
                return params;
            }

        };
        rq.add(postReq);
    }
    public void findUserById(final Compte compte) {

        RequestQueue rq = Volley.newRequestQueue(compte.getActivity());
        SharedPreferences settings = compte.getActivity().getSharedPreferences(Constante.PREFS_NAME, 0);
        int id = settings.getInt("id", 0);
        StringRequest postReq = new StringRequest(Request.Method.GET, ConstanteService.user+"?id="+id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    ObjectMapper mapper = new ObjectMapper();
                    Utilisateur utilisateur1 = mapper.readValue(response, Utilisateur.class);
                    compte.setView(utilisateur1);
                }
                catch(Exception e){
                    Log.e("TEST VOLLEY", "erreur : " + e.getMessage());
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        })  {

        };
        rq.add(postReq);
    }
    public void getSolde(final Compte compte) {

        RequestQueue rq = Volley.newRequestQueue(compte.getActivity());

        StringRequest postReq = new StringRequest(Request.Method.GET, ConstanteService.solde, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    ObjectMapper mapper = new ObjectMapper();
                    Double value = mapper.readValue(response, Double.class);
                    compte.setSolde(value.doubleValue());
                }
                catch(Exception e){
                    Log.e("TEST VOLLEY", "erreur : " + e.getMessage());
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        })  {


        };
        rq.add(postReq);
    }

    public void deconnexion(final MainActivity mainactivity) {
        RequestQueue rq = Volley.newRequestQueue(mainactivity);

        StringRequest postReq = new StringRequest(Request.Method.GET, ConstanteService.deconnexion, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    serviceG.deconnexion(mainactivity);
                    Intent intent = new Intent(mainactivity.getApplicationContext(), Connexion.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mainactivity.startActivity(intent);
                }
                catch(Exception e){
                    Log.e("TEST VOLLEY", "erreur : " + e.getMessage());
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        })  {

        };
        rq.add(postReq);
    }
}
