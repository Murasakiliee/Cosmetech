package mg.itu.mbds.cosmetech2.vue;

import android.app.Activity;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import mg.itu.mbds.cosmetech2.R;
import mg.itu.mbds.cosmetech2.modele.Achat;
import mg.itu.mbds.cosmetech2.service.AchatService;
import mg.itu.mbds.cosmetech2.vue.adapter.AchatAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListeAchat.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListeAchat#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListeAchat extends Fragment {

    private OnFragmentInteractionListener mListener;

    ArrayList<Achat> arrayList = new ArrayList<Achat>();
    SwipeRefreshLayout mSwipeRefreshLayout;
    ListView mListView;

    // TODO: Rename and change types and number of parameters
    public static ListeAchat newInstance() {
        ListeAchat fragment = new ListeAchat();
        return fragment;
    }

    public ListeAchat() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        shuffle();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_liste_produit, container, false);
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        mSwipeRefreshLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipeToRefreshAchat);
        mListView = (ListView) getActivity().findViewById(R.id.listViewAchat);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                shuffle();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void shuffle(){
        AchatService service = new AchatService();
        service.getAllAchats(this);
    }

    public void setData(Achat[] achats){
        if (achats==null || achats.length == 0){
            Toast.makeText(getActivity(), "Aucun achat effectue", Toast.LENGTH_SHORT).show();
        }
        arrayList = new ArrayList(Arrays.asList(achats));
        AchatAdapter adapter = new AchatAdapter(arrayList, this.getActivity(), this);
        mListView.setAdapter(adapter);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(2);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
