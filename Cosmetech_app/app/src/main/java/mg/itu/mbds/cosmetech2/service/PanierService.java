package mg.itu.mbds.cosmetech2.service;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

import mg.itu.mbds.cosmetech2.R;
import mg.itu.mbds.cosmetech2.modele.LigneCommande;
import mg.itu.mbds.cosmetech2.modele.Produit;
import mg.itu.mbds.cosmetech2.utilitaire.ConstanteService;
import mg.itu.mbds.cosmetech2.vue.Compte;
import mg.itu.mbds.cosmetech2.vue.MainActivity;
import mg.itu.mbds.cosmetech2.vue.Panier;

/**
 * Created by Murielle on 06/04/2017.
 */
public class PanierService {
    private VueService serviceG = new VueService();
    public void creerPanier(final MainActivity activity){
        RequestQueue rq = Volley.newRequestQueue(activity);

        StringRequest postReq = new StringRequest(Request.Method.GET, ConstanteService.creerPanier, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    ObjectMapper mapper = new ObjectMapper();
                    Integer idPanier = mapper.readValue(response, Integer.class);
                    serviceG.setIdpanier(activity, idPanier.intValue());
                    FragmentManager fragmentManager = activity.getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    Panier produit = new Panier();
                    fragmentTransaction.replace(R.id.container, produit).commit();

                }
                catch(Exception e){
                    Log.e("TEST VOLLEY", "erreur : "+e.getMessage());
                    serviceG.testConnexion(activity);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TEST VOLLEY", "erreur : "+error.getMessage());
                serviceG.erreurConnexion(activity);

            }
        })  {

        };
        rq.add(postReq);
    }

    public void getProduits (final Panier panier){
        RequestQueue rq = Volley.newRequestQueue(panier.getActivity());
        int idpanier = serviceG.getIdpanier(panier.getActivity());
        StringRequest postReq = new StringRequest(Request.Method.GET, ConstanteService.produits+"?idpanier="+idpanier, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    ObjectMapper mapper = new ObjectMapper();
                    LigneCommande[] commandes = mapper.readValue(response, LigneCommande[].class);
                    panier.setLigneCommandes(commandes);
                }
                catch(Exception e){
                    Log.e("TEST VOLLEY", "erreur : " + e.getMessage());
                    //erreur
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //erreur

            }
        })  {


        };
        rq.add(postReq);
    }

    public void getProduit (final mg.itu.mbds.cosmetech2.vue.Produit produitVue, final int id){
        RequestQueue rq = Volley.newRequestQueue(produitVue.getActivity());
        StringRequest postReq = new StringRequest(Request.Method.GET, ConstanteService.produit+"?codeproduit="+id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    ObjectMapper mapper = new ObjectMapper();
                    Produit produit = mapper.readValue(response, Produit.class);
                    produitVue.setProduit(produit);
                }
                catch(Exception e){
                    Log.e("TEST VOLLEY", "erreur : "+e.getMessage());
                    //erreur
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //erreur

            }
        })  {

        };
        rq.add(postReq);
    }

    public void checkStock (final Activity activity, final int codeproduit, final int quantite){
        RequestQueue rq = Volley.newRequestQueue(activity);
        String requete = "?codeproduit="+codeproduit+"&quantite="+quantite;
        StringRequest postReq = new StringRequest(Request.Method.GET, ConstanteService.checkStock+requete, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    ObjectMapper mapper = new ObjectMapper();
                    Boolean res = mapper.readValue(response, Boolean.class);
                    if (res){
                        addLigneCommande(activity, codeproduit, quantite);
                    }
                    else{
                        Toast.makeText(activity, "Stock epuise", Toast.LENGTH_SHORT).show();
                    }
                }
                catch(Exception e){
                    Log.e("TEST VOLLEY", "erreur : "+e.getMessage());
                    //erreur
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //erreur

            }
        })  {
        };
        rq.add(postReq);
    }

    public void addLigneCommande (final Activity activity, final int codeproduit, final int quantite){

        RequestQueue rq = Volley.newRequestQueue(activity);
        StringRequest postReq = new StringRequest(Request.Method.POST, ConstanteService.addLigneCommande, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    FragmentManager fragmentManager = activity.getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    Panier produit = new Panier();
                    fragmentTransaction.replace(R.id.container, produit).commit();
                }
                catch(Exception e){
                    Log.e("TEST VOLLEY", "erreur : "+e.getMessage());
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //erreur

            }
        })  {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap();
                int idpanier = serviceG.getIdpanier(activity);
                params.put("idpanier", new Integer(idpanier).toString());
                params.put("idproduit", new Integer(codeproduit).toString());
                params.put("qte", new Integer(quantite).toString());
                return params;
            }
        };
        rq.add(postReq);
    }

    public void validerCommande (final Activity activity, final double montant, final int idpanier){

        RequestQueue rq = Volley.newRequestQueue(activity);
        StringRequest postReq = new StringRequest(Request.Method.POST, ConstanteService.enregistrerTransaction, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    ObjectMapper mapper = new ObjectMapper();
                    Boolean res = mapper.readValue(response, Boolean.class);
                    if (res){
                        if(idpanier ==  serviceG.getIdpanier(activity)) {
                            serviceG.setIdpanier(activity, 0);
                        }
                        FragmentManager fragmentManager = activity.getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        Compte compte = Compte.newInstance();
                        fragmentTransaction.replace(R.id.container, compte).commit();
                    }
                    else{
                        Toast.makeText(activity, "Veuillez recharger votre compte", Toast.LENGTH_SHORT).show();
                    }

                }
                catch(Exception e){
                    Log.e("TEST VOLLEY", "erreur : "+e.getMessage());

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //erreur

            }
        })  {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap();
                Log.e("TEST VOLLEY", "test : " + idpanier + " " + montant);
                params.put("montant", new Double(montant).toString());
                params.put("idpanier", new Integer(idpanier).toString());
                return params;
            }
        };
        rq.add(postReq);
    }

}
