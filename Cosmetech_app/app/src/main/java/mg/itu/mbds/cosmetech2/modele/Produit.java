package mg.itu.mbds.cosmetech2.modele;

/**
 * Created by Murielle on 06/04/2017.
 */
public class Produit {
    private int id;
    private String nom;
    private String description;
    private String url;
    private double prix;
    private String categorie;

    public Produit(){}

    public Produit(int id, String nom, String description, String url, double prix, String idcategorie) {
        this.setId(id);
        this.setNom(nom);
        this.setDescription(description);
        this.setUrl(url);
        this.setPrix(prix);
        this.setCategorie(idcategorie);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String idcategorie) {
        this.categorie = idcategorie;
    }
}
