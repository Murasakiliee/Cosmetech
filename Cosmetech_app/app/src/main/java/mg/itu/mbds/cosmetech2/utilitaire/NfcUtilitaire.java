package mg.itu.mbds.cosmetech2.utilitaire;

import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Parcelable;

/**
 * Created by Murielle on 06/04/2017.
 */
public class NfcUtilitaire {
    private String getDataByTag(Intent intent){
        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        NdefMessage[] msgs;
        String resultat = "";
        //Boucle sur les enregistrements
        if (rawMsgs != null)
        {
            msgs = new NdefMessage[rawMsgs.length];
            for (int i = 0; i < rawMsgs.length; i++)
            {
                msgs[i] = (NdefMessage) rawMsgs[i];
                NdefRecord record = msgs[i].getRecords()[i];
                resultat += new String(record.getPayload());
            }
        }
        return resultat;
    }
    public String[] getData (Intent intent){
        String message = getDataByTag(intent);
        String[] data = new String[2];
        //0 : produit ou panier - 1 : autre
        data = message.split("P");
        if (data[0] != null){
            if (data[0].compareTo("0")==0) data[0]="Panier";
            else data[0] = "Produit";
        }
        return data;
    }
}
