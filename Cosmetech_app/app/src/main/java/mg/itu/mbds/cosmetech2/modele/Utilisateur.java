package mg.itu.mbds.cosmetech2.modele;

/**
 * Created by Murielle on 06/04/2017.
 */
public class Utilisateur {
    private int id;
    private String nom;
    private String prenom;
    private String pseudo;
    private String motDePasse;

    public Utilisateur(){

    }

    public Utilisateur(int id, String nom, String prenom, String pseudo, String motdepasse) {
        this.setId(id);
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setPseudo(pseudo);
        this.setMotDePasse(motdepasse);
    }

    public Utilisateur(String pseudo, String motDePasse) {
        this.setPseudo(pseudo);
        this.setMotDePasse(motDePasse);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }
}
