package mg.itu.mbds.cosmetech2.vue;

import android.app.Activity;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import mg.itu.mbds.cosmetech2.R;
import mg.itu.mbds.cosmetech2.modele.LigneCommande;
import mg.itu.mbds.cosmetech2.service.PanierService;
import mg.itu.mbds.cosmetech2.service.VueService;
import mg.itu.mbds.cosmetech2.vue.adapter.ProduitAdapter;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Panier.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Panier#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Panier extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "idPanier";

    ArrayList<LigneCommande> arrayList = new ArrayList<LigneCommande>();
    SwipeRefreshLayout mSwipeRefreshLayout;
    ListView mListView;
    Button valider;
    private TextView txtMontant;

    private VueService serviceG = new VueService();


    private OnFragmentInteractionListener mListener;

    // TODO: Rename and change types and number of parameters
    public static Panier newInstance(int id) {
        Panier fragment = new Panier();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, id);
        fragment.setArguments(args);
        return fragment;
    }

    public Panier() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int mParam1= serviceG.getIdpanier(getActivity());
        if (mParam1 > 0){
            PanierService service = new PanierService();
            service.getProduits(this);
        }

    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        mSwipeRefreshLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipeToRefresh);
        mListView = (ListView) getActivity().findViewById(R.id.listView);

        //montantPanier
        txtMontant = (TextView) getActivity().findViewById(R.id.montantPanier);

        valider = (Button) getActivity().findViewById(R.id.bttConfirmer);
        if (serviceG.getIdpanier(getActivity())>0){
            valider.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shuffle();
                    double montant = calculateMontant();
                    new PanierService().validerCommande(getActivity(), montant,serviceG.getIdpanier(getActivity()));
                }
            });

        }

        mSwipeRefreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                shuffle();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_panier, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(3);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public void shuffle(){
        PanierService service = new PanierService();
        service.getProduits(this);
    }

    public void setLigneCommandes(LigneCommande[] lignes){
        if (lignes==null || lignes.length == 0){
            Toast.makeText(getActivity(), "Aucune commande enregistree", Toast.LENGTH_SHORT).show();
        }
        arrayList = new ArrayList(Arrays.asList(lignes));
        ProduitAdapter adapter = new ProduitAdapter(arrayList, this.getActivity());
        mListView.setAdapter(adapter);
        calculateMontant();
        if(arrayList.size()>0) valider.setVisibility(View.VISIBLE);

    }

    public double calculateMontant(){
        double montant = 0;
        if (arrayList!= null && arrayList.size()>0){
            for (int i=0; i<arrayList.size(); i++){
                montant += arrayList.get(i).getPrix()*arrayList.get(i).getQuantite();
            }
        }
        txtMontant.setText("Montant : "+montant);
        return montant;
    }
}
