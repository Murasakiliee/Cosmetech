package mg.itu.mbds.cosmetech2.utilitaire;

/**
 * Created by Murielle on 06/04/2017.
 */
public class ConstanteService {
    static String host = "https://cosmetech.000webhostapp.com/index.php/";

    public static String users = host+"client/getAllUsers";
    public static String connexion = host+"client/findUser";
    public static String deconnexion = host+"client/logout";
    public static String user = host+"client/getUser";
    public static String solde = host+"client/getMonney";

    public static String produit = host+"produit/getProduits";
    public static String produits = host+"achat/getLigneCommandeByIdPanier";
    public static String checkStock = host+"produit/verifierQuantiteStock";

    public static String creerPanier = host+"achat/creerPanier";
    public static String addLigneCommande = host+"achat/ajoutLigneCommande";
    public static String enregistrerTransaction = host+"achat/enregistrerTransaction";
    public static String allAchats = host+"achat/listeAchats";
    public static String enregistrerLivraison = host+"achat/confirmerLivraison";
}
