package mg.itu.mbds.cosmetech2.vue.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import mg.itu.mbds.cosmetech2.R;
import mg.itu.mbds.cosmetech2.modele.Achat;
import mg.itu.mbds.cosmetech2.service.AchatService;
import mg.itu.mbds.cosmetech2.service.PanierService;
import mg.itu.mbds.cosmetech2.vue.ListeAchat;

/**
 * Created by Murielle on 29/04/2017.
 */
public class AchatAdapter extends BaseAdapter {
    private ArrayList<Achat> items;
    Context mContext;
    ListeAchat achatFragment;

    public AchatAdapter(ArrayList<Achat> data, Context context, ListeAchat achatFragment) {
        this.mContext = context;
        this.items = data;
        this.achatFragment = achatFragment;
    }

    @Override
    public int getCount() {
        return items.size(); //returns total of items in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // inflate the layout for each list row
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).
                    inflate(R.layout.template_achat, parent, false);
        }

        // get current item to be displayed
        final Achat currentItem = (Achat) getItem(position);

        // get the TextView for item name and item description
        TextView textViewItemName = (TextView)
                convertView.findViewById(R.id.dateAchat);
        TextView textViewItemDescription = (TextView)
                convertView.findViewById(R.id.montantAchat);
        SimpleDateFormat formater = new SimpleDateFormat("dd MMMM yyyy");
        textViewItemName.setText(formater.format(currentItem.getDateachat()));
        textViewItemDescription.setText("Montant : "+currentItem.getMontant());

        Button button = (Button) convertView.findViewById(R.id.livraison);
        if (currentItem.getStatut()==-1){
            button.setText("Payer");
            button.setBackground(achatFragment.getActivity().getResources().getDrawable(R.drawable.mybutton));
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new PanierService().validerCommande(achatFragment.getActivity(), currentItem.getMontant(), currentItem.getId());
                }
            });
        }
        else if (currentItem.getStatut()==1){
            button.setText("Livrer");
            button.setBackground(achatFragment.getActivity().getResources().getDrawable(R.drawable.mybutton2));
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AchatService().enregistrerLivraison(achatFragment, currentItem.getId());
                }
            });
        }
        else{
            button.setBackground(achatFragment.getActivity().getResources().getDrawable(R.drawable.mybutton3));
            button.setText("Livre");
            button.setActivated(false);
        }


        // returns the view for the current row
        return convertView;
    }

}
