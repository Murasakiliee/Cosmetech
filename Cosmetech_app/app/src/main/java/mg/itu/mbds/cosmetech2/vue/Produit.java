package mg.itu.mbds.cosmetech2.vue;

import android.app.Activity;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import mg.itu.mbds.cosmetech2.R;
import mg.itu.mbds.cosmetech2.service.PanierService;
import mg.itu.mbds.cosmetech2.service.VueService;
import mg.itu.mbds.cosmetech2.utilitaire.Configuration;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Produit.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Produit#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Produit extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "idProduit";

    // TODO: Rename and change types of parameters
    private int idProduit;
    private TextView txtNom;
    private TextView txtCategorie;
    private TextView txtPrix;
    private TextView txtDescription;
    private EditText edtQuantite;
    private Button btAcheter;
    private String lienVideo = null;

    private OnFragmentInteractionListener mListener;

    private YouTubePlayerView myouTubePlayerView;
    private YouTubePlayer.OnInitializedListener monInitializedListener;

    private VueService serviceG = new VueService();

    // TODO: Rename and change types and number of parameters
    public static Produit newInstance(int param1) {
        Produit fragment = new Produit();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public Produit() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.idProduit = getArguments().getInt(ARG_PARAM1);
            //maka anle produit en ligne
            new PanierService().getProduit(this, idProduit);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_produit, container, false);
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        txtNom = (TextView) this.getActivity().findViewById(R.id.nom);
        txtCategorie = (TextView) this.getActivity().findViewById(R.id.categorie);
        txtPrix = (TextView) this.getActivity().findViewById(R.id.prix);
        txtDescription = (TextView) this.getActivity().findViewById(R.id.description);
        edtQuantite = (EditText)this.getActivity().findViewById(R.id.edtQuantite);
        btAcheter = (Button)this.getActivity().findViewById(R.id.acheter);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(0);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public int getIdProduit() {
        return idProduit;
    }

    public void setProduit(mg.itu.mbds.cosmetech2.modele.Produit produit){


        if(produit.getNom()!=null)txtNom.setText(produit.getNom());
        if(produit.getCategorie()!=null)txtCategorie.setText(produit.getCategorie());
        if(new Double(produit.getPrix())!=null)txtPrix.setText(new Double(produit.getPrix()).toString());
        if(produit.getDescription()!=null)txtDescription.setText(produit.getDescription());

        btAcheter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(serviceG.checkHavePanier(getActivity())){
                    if(myouTubePlayerView != null){
                        myouTubePlayerView = null;
                    }
                    new PanierService().checkStock(getActivity(), getIdProduit(), getQuantite());
                }
                else{
                    Toast.makeText(getActivity(), "Veuillez tagger a l'entree le tag pour avoir un panier", Toast.LENGTH_LONG).show();
                }
            }
        });

        lienVideo = produit.getUrl();
        myouTubePlayerView = (YouTubePlayerView) this.getActivity().findViewById(R.id.youtube_player_view);
        monInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.loadVideo(lienVideo);
            }
            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };
        myouTubePlayerView.initialize(Configuration.DEVELOPER_KEY, monInitializedListener);
    }

    public int getQuantite(){
        return new Integer(this.edtQuantite.getText().toString());
    }
}
