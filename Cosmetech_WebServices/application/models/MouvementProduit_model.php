<?php  if ( ! defined('BASEPATH')) exit('No direct script acces allowed');
class MouvementProduit_model extends CI_Model
{
    protected $table = 'mouvementstock';
	public function __construct() 
    {
           parent::__construct(); 
           $this->load->database();
    }
    
	public function verifierQuantiteStock($idproduit , $qte){
		$data = $this->db->select("idproduit")
            ->from(" (SELECT idproduit , sum(entree) - sum(sortie) as nb  from mouvementstock group by idproduit) as req")
			->where(" req.nb >  ".$qte." and req.idproduit =  ".$idproduit)
            ->get()
            ->result();
			if($data) return true;
			return false;
	}
	public function retrait($id){
  $listeCommande = $this->db->select("p.* , l.quantite , l.montant ")->from(" produit p , lignecommande l ")->where(" p.id = l.idproduit and l.idpanier = ".$id) ->get()->result();
for($o = 0 ; $o < count($listeCommande) ; $o ++) {
 $this->db->set('idproduit',  $listeCommande[$o]->id);
		$this->db->set('entree', 0);
		$this->db->set('sortie',  $listeCommande[$o]->quantite);
		$this->db->set('datemouvement',  'NOW()', FALSE);
         $this->db->insert($this->table);
}

	}
}