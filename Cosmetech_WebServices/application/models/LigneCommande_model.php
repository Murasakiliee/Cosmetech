<?php  if ( ! defined('BASEPATH')) exit('No direct script acces allowed');
class LigneCommande_model extends CI_Model
{
    protected $table = 'lignecommande';
	public function __construct() 
    {
           parent::__construct(); 
           $this->load->database();
		   $this->load->model('Panier_model','Panier');
$this->load->model('Produit_model','Produit');
    }
	 // Inserer une ligne commande
    public function ajoutLigneCommande($idpanier,$idproduit,$qte)
    {
		$prix = $this->Produit->getPrixProduit($idproduit);
$mont = $this->db->query('select montant from panier where id = '.$idpanier)->result()[0]->montant;
       	$this->db->set('montant',  $mont+$prix*$qte);
		$this->db->where(' id = '.$idpanier);
        $this->db->update('panier');
		 
        $this->db->set('idpanier', $idpanier );
		$this->db->set('idproduit', $idproduit );
		$this->db->set('quantite', $qte );
		$this->db->set('montant',  $prix*$qte);
		$this->db->insert($this->table);
    }
    public function getLigneCommande($idp){
		$where="c.idpanier =".$idp." and c.idproduit = p.id";
        	$data = $this->db->select("c.id as idcommande, p.id as idproduit , p.nom , c.quantite , p.prix ")
            ->from(" lignecommande c , produit p")
            ->where($where)
            ->get()
            ->result();
           return $data;
	}
public function annulerCommande($idpanier, $cl){

}
}