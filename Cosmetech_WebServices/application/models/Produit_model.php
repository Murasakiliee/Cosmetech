<?php  if ( ! defined('BASEPATH')) exit('No direct script acces allowed');
class Produit_model extends CI_Model
{
    protected $table = 'produit';
	public function __construct() 
    {
           parent::__construct(); 
           $this->load->database();
    }
    // Inserer un produit
    public function inserer_produit($libelle,$description,$url,$prix)
    {
        $this->db->set('libelle',  $libelle);
		$this->db->set('description',  $description);
		$this->db->set('url',  $url);
		$this->db->set('prix',  $prix);
        return $this->db->insert($this->table);
    }
  
    public function getProduit($id)
    {
        $data = $this->db->select("p.id,p.nom,p.description,p.url,p.prix,c.libelle as categorie")
            ->from("produit p, categorie c")
			 ->where("p.idcategorie = c.id and p.id = ".$id)
            ->get()
            ->result();
return $data[0];
    }	
 public function getPrixProduit($id)
    {
        $data = $this->db->select("prix")
            ->from("produit ")
			 ->where("id = ".$id)
            ->get()
            ->result();
return $data[0]->prix;
    }	
	
}