<?php  if ( ! defined('BASEPATH')) exit('No direct script acces allowed');
class Client_model extends CI_Model
{
    protected $table = 'client';
	public function __construct() 
    {
           parent::__construct(); 
           $this->load->database();
    }
    // Inserer un client
    public function inserer_client($nom,$prenom,$pseudo,$motDePasse)
    {
        $this->db->set('nom',  $nom);
		$this->db->set('prenom',  $prenom);
		$this->db->set('pseudo',  $pseudo);
		$this->db->set('motDePasse',  $motDePasse);
        return $this->db->insert($this->table);
    }
    // Supprimer un client
    public function supprimer_client($id)
    {
        return $this->db->where('id', (int) $id)
            ->delete($this->table);
    }
    
    public function count($where = array())
    {
        return (int) $this->db->where($where)
            ->count_all_results($this->table);
    }
    // Retourne une liste des clients
    public function liste_client()
    {
        return $this->db->select("*")
            ->from("client")
            ->get()
            ->result();
    }
	
    public function chercher_client_selonid($id)
    {
        $idd="$id";
        $where="id = $idd ";
       $data =  $this->db->select("*")
            ->from("client")
            ->where($where)
            ->get()
            ->result();
 	return $data[0]->id;
    }
 public function chercher_client_id($id)
    {
        $idd="$id";
        $where="id = $idd ";
       $data =  $this->db->select("*")
            ->from("client")
            ->where($where)
            ->get()
            ->result();
 	return $data[0];
    }
    public function chercher_client($login , $mdp)
    {
        $where="c.pseudo like '".$login."' and motDePasse like '".$mdp."'";
        $data = $this->db->select("*")
            ->from("client c")
            ->where($where)
            ->get()
            ->result();
           return $data[0];
    }
}