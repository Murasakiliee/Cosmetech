<?php  if ( ! defined('BASEPATH')) exit('No direct script acces allowed');
class MouvementPortefeuille_model extends CI_Model
{
    protected $table = 'mouvementportefeuille';
	public function __construct() 
    {
           parent::__construct(); 
           $this->load->database();
    }
    // Debiter un compte
    public function debiter($debit)
    {
        $this->db->set('debit',  $debit);
		$this->db->set('credit',  0);
		$this->db->set('datemouvement',  'NOW()',FALSE);
		return $this->db->insert($this->table);
    }
    // Crediter un compte
   public function crediter($credit,$idcl)
    {
        $this->db->set('credit',  $credit);
		$this->db->set('debit',  0);
                $this->db->set('idclient',  $idcl);
		$this->db->set('datemouvement',  'NOW()',FALSE);
		return $this->db->insert($this->table);
    }
   
    public function verifierCompte($montant,$id)
    {
		$solde = $this->avoirmontantsolde($id);
		if($solde->monnaie >= $montant)return true;
		else return false;
    }
	public function avoirmontantsolde($id)
    {
        $idd="$id";
        $where="m.idclient = $idd and c.id= m.idclient";
        return $this->db->select("m.idclient ,c.nom, sum(debit)-sum(credit) as monnaie")
            ->from("mouvementportefeuille m,client c")
            ->where($where)
            ->get()
            ->result()[0];
    }
}