<?php  if ( ! defined('BASEPATH')) exit('No direct script acces allowed');
class Panier_model extends CI_Model
{
    protected $table = 'panier';
	public function __construct() 
    {
           parent::__construct(); 
           $this->load->database();
    }
    // Inserer un panier
    public function creerPanier($idcl)
    {
        $this->db->set('idclient',  $idcl);
$this->db->set('montant',  0);
$this->db->set('statut',  -1);
		$this->db->set('dateachat',  'NOW()', FALSE );
		$this->db->insert($this->table);
		$data = $this->db->query('select max(p.id) as id from panier p')->result();
		return $data[0]->id;
    }
	 // enregistrer transaction
    public function enregistrerTransaction($id,$montant)
    {
   $where = 'id='.$id;
		$this->db->set('statut',  1);
$this->db->set('montant',$montant  );
		$this->db->where($where);
       $this->db->update($this->table);


    }
	// confirmation livraison
    public function confirmerLivraison($id,$idp)
    {
        $idpan = $data = $this->db->query('select max(id) as id from panier where statut=1 and idclient = '.$id." and id = ".$idp)->result()[0]->id;
        $where = 'id='.$idpan;
		$this->db->set('statut',  2);
		$this->db->where($where);
        return $this->db->update($this->table);
    }
	public function getPanierClient($idcl){
		$where = " idclient = ".$idcl;
$data = $this->db->select("max(id) as idp")
            ->from(" panier ")
            ->where($where)
            ->get()
            ->result();
           return $data[0]->idp;
	}
public function listeAchats($idcl){
		$where = " idclient = ".$idcl;
$data = $this->db->select("id ,DATE_FORMAT(dateachat ,'%Y-%m-%d') AS dateachat,  montant ,statut")
            ->from(" panier ")
            ->where($where)
->order_by('dateachat','desc')
            ->get()
            ->result();
           return $data;
	}

  }