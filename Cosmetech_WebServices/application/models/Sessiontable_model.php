<?php  if ( ! defined('BASEPATH')) exit('No direct script acces allowed');
class Sessiontable_model extends CI_Model
{
    protected $table = 'sessiontable';
	public function __construct() 
    {
           parent::__construct(); 
           $this->load->database();
    }
    // Insertion
    public function insert($id)
    {
$this->db->empty_table($this->table);

       	$this->db->set('idclient',  $id);
        return $this->db->insert($this->table);
    }
    
	
    public function getId()
    {
       $data =  $this->db->select("idclient")
            ->from($this->table)
            ->get()
            ->result();

 	return $data;
    }
public function delete(){
$this->db->empty_table($this->table);
}
 
}