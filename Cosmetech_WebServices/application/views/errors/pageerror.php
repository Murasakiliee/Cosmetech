<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Call - Center - Erreur</title>
	<link href="<?php echo css_url('bootstrap.min');?>" rel="stylesheet"  type="text/css"/>
	<link href="<?php echo css_url('styles');?>" rel="stylesheet"  type="text/css"/>
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
						<div class="panel-body">
							Vous n'avez pas acc&egrave;s &agrave; cette page.Veuillez vous connecter en tant qu'administrateur.
							</div>
					</div>
				</div>
			</div>
		</div><!--/.row-->
	</div><!--/.main-->
<script src="<?php echo js_url('jquery-1.11.1.min');?>" type="text/javascript"></script>
<script src="<?php echo js_url('bootstrap.min');?>" type="text/javascript"></script>
</body>
</html>
