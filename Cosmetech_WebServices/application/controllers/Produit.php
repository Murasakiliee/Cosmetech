<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class produit extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
		$this->load->model('Produit_model','Produit');
		$this->load->model('MouvementProduit_model','MouvementProduit');
		$this->load->library('session');
    }
	public function getProduits(){
		//Avoir l'information sur un produit
		if(isset($_GET['codeproduit'])){
			$rep=$this->Produit->getProduit($_GET['codeproduit']);
			echo json_encode($rep, JSON_NUMERIC_CHECK);
		}
	}

	public function verifierQuantiteStock(){
	// QuantitÃÂÃÂ© d'un produit dans le stock suffisante
		if(isset($_GET['codeproduit']) && isset($_GET['quantite']) ){
			$rep=$this->MouvementProduit->verifierQuantiteStock($_GET['codeproduit'],$_GET['quantite']);
			echo json_encode($rep, JSON_NUMERIC_CHECK);
		}
	}
}