<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class achat extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
		$this->load->model('Panier_model','Panier');
		$this->load->model('LigneCommande_model','LigneCommande');
		$this->load->model('MouvementPortefeuille_model','MouvementPortefeuille');
$this->load->model('MouvementProduit_model','MouvementProduit');
		$this->load->model('Sessiontable_model','Sessiontable');
		$this->load->library('session');
               if($this->Sessiontable->getId()!=null){ 
                    $vari = $this->Sessiontable->getId()[0]->idclient;
                    $this->session->set_userdata('client',$vari);
               }
    }
	public function creerPanier()
	{
		if($this->session->userdata('client')!=null){
			$cl = $this->session->userdata('client');
			//inserer une ligne dans la table panier
			echo $this->Panier->creerPanier($cl);
		}
	}
	public function ajoutLigneCommande()
	{
		//inserer une ligne dans la table panier
			if(isset($_POST['idpanier']))
			{
				if($this->session->userdata('client')!=null){
				$this->LigneCommande->ajoutLigneCommande($_POST['idpanier'],$_POST['idproduit'],$_POST['qte']);
				}
			}	
	}
	public function enregistrerTransaction(){
		if($this->session->userdata('client')!=null){
			$cl = $this->session->userdata('client');
			//appel verifierCompte , 
			$oui = $this->MouvementPortefeuille->verifierCompte($_POST['montant'],$cl);
			if($oui){
			//Retrait ( quantite en stock )
			$this->MouvementProduit->retrait($_POST['idpanier']);
			// champ statut dans panier devient paye
			$this->Panier->enregistrerTransaction($_POST['idpanier'],$_POST['montant']);
			//insertion dans mouvement portefeuille
			$this->MouvementPortefeuille->crediter($_POST['montant'],$cl);

			}
echo $oui;
		}
else echo 0;
	}	
	public function confirmerLivraison(){
		//champ statut dans panier devient livre
		if($this->session->userdata('client')!=null){
			$cl = $this->session->userdata('client');
			echo $this->Panier->confirmerLivraison($cl,$_GET['idpanier']);
		}
	}
	public function getLigneCommandeByIdPanier(){
		//Avoir l'information sur un panier
		if(isset($_GET['idpanier'])){
			$rep=$this->LigneCommande->getLigneCommande($_GET['idpanier']);
			echo json_encode($rep, JSON_NUMERIC_CHECK);
		}
	}
public function listeAchats(){
if($this->session->userdata('client')!=null){
			$cl = $this->session->userdata('client');
			$rep = $this->Panier->listeAchats($cl);
echo json_encode($rep, JSON_NUMERIC_CHECK);
		}
}
public function annulerCommande(){
if($this->session->userdata('client')!=null){
			$cl = $this->session->userdata('client');
			$rep = $this->LigneCommande->annulerCommande($_GET['idpanier'],$cl);
		}
}	
}