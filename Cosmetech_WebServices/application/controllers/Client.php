<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class client extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
		$this->load->model('Client_model','Client');
		$this->load->model('MouvementPortefeuille_model','MouvementPortefeuille');
                $this->load->model('Sessiontable_model','Sessiontable');
		$this->load->library('session');
               if($this->Sessiontable->getId()!=null){ 
				   $vari = $this->Sessiontable->getId()[0]->idclient;
	               $this->session->set_userdata('client',$vari);
		}
    }
	public function index(){
		$this->getAllUsers();
	}
	public function findUser(){
		if(isset($_POST['login']) && isset($_POST['mdp'])){
			$data=$this->Client->chercher_client($_POST['login'],$_POST['mdp']);		
			$vari = $this->Sessiontable->insert($data->id);
			$this->session->set_userdata('client',$data->id);
			echo json_encode($data, JSON_NUMERIC_CHECK);
		}
	}
	public function getUser(){
		if(isset($_GET['id'])){
		$data=$this->Client->chercher_client_id($_GET['id']);
		echo json_encode($data, JSON_NUMERIC_CHECK);
		}
	}
	public function getAllUsers(){ 
			$data=$this->Client->liste_client();
			echo json_encode($data, JSON_NUMERIC_CHECK);
		
	}
	public function getMonney(){
		if($this->session->userdata('client')!=null){
			$cl = $this->session->userdata('client');
			$montant=$this->MouvementPortefeuille->avoirmontantsolde($cl)->monnaie;
			echo json_encode($montant, JSON_NUMERIC_CHECK);
		}
	}
	public function checkCount() {
		if($this->session->userdata('client')!=null && isset($_GET['solde'])){
			$cl = $this->session->userdata('client');
			$montant=$this->MouvementPortefeuille->verifierCompte($_GET['solde'],$cl);
			echo json_encode($montant, JSON_NUMERIC_CHECK);
		}
		return false;
	}
	public function logout(){
		$this->session->unset_userdata('client');
		$this->session->sess_destroy();
		$this->Sessiontable->delete();
	}
}